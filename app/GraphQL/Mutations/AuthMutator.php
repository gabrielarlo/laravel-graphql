<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

use Illuminate\Support\{
    Facades\Auth,
    Facades\Hash,
    Arr,
    Str
};

final class AuthMutator
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        $creds = Arr::only($args, ['email', 'password']);

        $user = User::where(['email' => $creds['email']])->first();
        if (!$user) return null;

        if (!Hash::check($creds['password'], $user->password)) return null;
        
        $token = Str::random(60);

        $user->api_token = $token;
        $user->save();

        return $token;
    }
}
