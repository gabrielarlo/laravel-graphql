<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SeedUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $faker = Factory::create();
        $password = bcrypt('secret');

        User::firstOrCreate([
            'name' => $faker->name,
            'email' => 'graphql@test.com',
            'password' => $password,
        ]);

        for ($i = 0; $i < 10; $i++) {
            User::firstOrCreate([
                'name' => $faker->name,
                'email' => $faker->email(),
                'password' => $password,
            ]);
        }
    }
}
